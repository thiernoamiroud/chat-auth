package com.chatauth.chatauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatAuthApplication.class, args);
	}

}
